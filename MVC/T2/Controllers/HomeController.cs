﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using T2.Models;

namespace T2.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            MDL info = new MDL();
            info.Id = 1;
            info.Name = "我是谁啊";
            ViewBag.msg = info.Id;
            ViewBag.name = info.Name;
            TempData["id"] = info.Id;
            TempData["name"] = info.Name;
            return View();
        }
        public ActionResult About()
        {

            return View();
        }
    }
}