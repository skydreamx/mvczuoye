﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using T2_4.Models;

namespace T2_4.Controllers
{
    public class HomeController : Controller
    {
        SchoolEntities db;
        public HomeController()
        {
            db = new SchoolEntities();
        }

        // GET: Home
        public ActionResult Index()
        {
            return View(db.Courses.ToList());
        }
        public ActionResult Detail(int id)
        {
            return View(db.Courses.Find(id));
        }

        public ActionResult Del(int id )
        {
            var info=db.Courses.Find(id);
            var del = db.Courses.Remove(info);
            db.SaveChanges();
            return RedirectToAction("index");
        }
        public ActionResult Modify(int id)
        {
            return View(db.Courses.Find(id));
        }
        [HttpPost]
        public ActionResult Modify()
        {
            int id = Convert.ToInt32( Request["CourseID"]);
            var title = Request["title"];
            var res = db.Courses.Find(id);
            res.Title = title;
            db.SaveChanges();
            return RedirectToAction("index");
        }
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Course course)
        {
            var title = Request["title"];
            course.Title = title;

            var credits = Request["Credits"];
            course.Credits = Convert.ToInt32(credits);

            var departmentID = Request["DepartmentID"];
            course.DepartmentID = Convert.ToInt32(departmentID);

            db.Courses.Add(course);
            db.SaveChanges();
            return RedirectToAction("index");

        }
    }
}