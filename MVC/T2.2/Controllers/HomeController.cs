﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using T2._2.Models;

namespace T2._2.Controllers
{
    public class HomeController : Controller
    {
        SchoolEntities db = new SchoolEntities();
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.list = db.Courses.ToList();
            return View();
        }
    }
}